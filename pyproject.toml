[tool.poetry]
name = "exacheck"
description = "ExaCheck - ExaBGP Health Checker"
authors = ["Chris <info@exacheck.net>"]
version = "0.0.7"
readme = "README.md"
homepage = "https://exacheck.net"
repository = "https://github.com/exacheck/exacheck"

[tool.poetry.dependencies]
# Python version required
python = "~3.11"

# Main requirements
exabgp = "^4.2.21"
loguru = "^0.7.2"
pydantic = "^2.5.3"
pyyaml = "^6.0.1"
setproctitle = "^1.3.3"
tabulate = "^0.9.0"
ujson = "^5.9.0"
apprise = "^1.7.2"

# For health checks
dnspython = "^2.5.0"
icmplib = "^3.0.4"
ntplib = "^0.4.0"
requests = "^2.31.0"

# For use with CLI scripts
click = "^8.1.7"

# Optional requirements
autopep8 = {version = "^2.0.4", optional = true}
black = {version = "^24.1.1", optional = true}
Cython = {version = "^3.0.8", optional = true}
devtools = {extras = ["pygments"], version = "^0.12.2", optional = true}
flake8 = {version = "^7.0.0", optional = true}
ipython = {version = "^8.20.0", optional = true}
Markdown = {version = "^3.5.2", optional = true}
mypy = {version = "^1.8.0", optional = true}
pipenv = {version = "^2023.11.15", optional = true}
pycodestyle = {version = "^2.11.1", optional = true}
Pygments = {version = "^2.17.2", optional = true}
pylint = {version = "^3.0.3", optional = true}
rich = {version = "^13.7.0", optional = true}
ruff = {version = "^0.1.13", optional = true}

# For type checking
pyre-check = {version = "^0.9.19", optional = true}
types-pyyaml = {version = "^6.0.12.12", optional = true}
types-requests = "^2.31.0.20240125"
types-tabulate = {version = "^0.9.0", optional = true}
types-ujson = {version = "^5.9.0", optional = true}
pylint-pydantic = {version = "^0.3.2", optional = true}

# For Sentry
sentry-sdk = {version = "^1.39.2", optional = true}

# For testing
hypothesis = {extras = ["cli"], version = "^6.94.0", optional = true}

[tool.poetry.dev-dependencies]
ipy = "^1.01"
pytest = "^8.0.0"
pytest-html = "^4.1.1"
semgrep = "^1.58.0"

[tool.poetry.extras]
formatting = [
  "autopep8",
  "black",
  "flake8",
  "pycodestyle",
  "pylint",
]
vscode = [
  "Cython",
  "devtools",
  "ipython",
  "Markdown",
  "mypy",
  "pipenv",
  "Pygments",
  "requests",
  "rich",
  "ruff",
]
typing = [
  "types-pyyaml",
  "types-tabulate",
  "types-ujson",
  "pyre-check",
  "pylint-pydantic",
]
sentry = [
  "sentry-sdk",
]
testing = [
  "hypothesis",
]

[tool.poetry.scripts]
exacheck = 'exacheck.cli:cli'

[build-system]
build-backend = "poetry.core.masonry.api"
requires = ["poetry-core>=1.0.0"]

[tool.autopep8]
# Raise default maximum line length
max_line_length = 120

[tool.flake8]
# Raise default maximum line length
max-line-length = 120

[tool.pylint.'MESSAGES CONTROL']
max-line-length = 120
extension-pkg-whitelist = "pydantic"

[tool.ruff]
line-length = 120
